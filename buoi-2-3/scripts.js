/*
Bài 1: Tính tiền lương nhân viên

Lương của nhân viên 1 ngày là 100.000
Số ngày làm việc của nhân viên là 30 ngày

Bước 1: Tạo biến luong1Ngay, soNgayLam
Bước 3: Tính tổng lương với số tiền và ngày làm của nhân viên
Bước 3: In kết quả theo biểu mẫu ra console

Kết quả số tiền của nhân viên nhận được 


*/

document.getElementById('luong').onclick = function(){
    var luong1ngay = document.getElementById("luong1ngay").value;
    var songngay = document.getElementById("songaylam").value;
    var tongluong = luong1ngay * songngay;

    document.getElementById("tongluong").innerHTML = tongluong;
}

/*
Bài 2: Tính giá trị trung bình

Giá trị 5 số thực

Bước 1: Tìm tới thẻ và lấy giá trị
Bước 2: Tạo hàm cơ bản
Bước 3: Gắn sự kiện click cho button
Bước 4: Thay đổi nội dung cho thẻ
Bước 5: Xử lý load trang với button của form
Bước 6: Xử lý ép kiểu dữ liệu


Kết quả giaTriTrungBinh 
*/

document.getElementById('tongtrungbinh').onclick = function(){
    let inputElement = document.querySelectorAll(".bai2 .form-control");
   let sum = 0;
  for(let i = 0;i < inputElement.length;i++ ){
    sum += Number(inputElement[i].value);
   }
   document.getElementById("ketquatrungbinh").innerHTML = sum/inputElement.length;

}

/*
Bài 3: Quy đổi tiền

Giá USD hiện nay = 23.500VND

Bước 1: Tạo biến giaVND, soluongUSD, giaUSD
Bước 2: Quy đổi số lượng USD sang VND
Bước 3: In kết quả theo biểu mẫu

Kết quả USD người dùng nhập đổi sang VND

*/

document.getElementById("quydoi").onclick = function(){
    var usd = document.getElementById("usd").value;
    var vnd = new Intl.NumberFormat('VN').format(usd * 23500);
    document.getElementById("menhgia").innerHTML = vnd;
}


/*
Bài 4: Tính diện tích, chu vi hình chữ nhật

Cho chiều dài là 5 Cho chiều rộng là 3

Bước 1: Tạo biến chieuDai, chieuRong, chuVi, dienTich
Bước 2: Tính công thức diện tích và chu vi
Bước 3: In 2 kết quả (chu vi và diện tích) ra console

Kết quả chuvi dienTich (chu vi và diện tích)

*/

document.getElementById("tinhtongchuvidientich").onclick = function(){
    var chieuRong = document.getElementById("chieurong").value;
    var chieuDai = document.getElementById("chieudai").value;
    
    var dienTich = chieuRong * chieuDai;
    var chuvVi = (Number(chieuRong)+Number(chieuDai)) * 2;
    
    document.getElementById("tongchuvidientich").innerHTML = `
        Diện tích: ${dienTich};
        Chu vi: ${chuvVi}
    `;

}

/*
Bài 5: Tính tổng 2 ký số

Mô hình 3 khối:
Số nguyên n có 2 ký số 

Bước 1: Tạo biến n, hangChuc, donVi
Bước 2: Tách số hàng chục theo công thức hangChuc = Math.floor (n/10)
Bước 3: Tác số hàng đơn vị theo công thức donVi = Math.floor (n % 10);
Bước 4: In kết quả tong2KySo 

Kết quả tong2KySo (tổng 2 ký số)
*/

document.getElementById("tinhtong2kyso").onclick = function(){
    var n = document.getElementById("number").value;

    var hangChuc = Math.floor(n/10);
    var donVi = n%10;

    document.getElementById("tong2kyso").innerHTML = hangChuc + donVi;

}